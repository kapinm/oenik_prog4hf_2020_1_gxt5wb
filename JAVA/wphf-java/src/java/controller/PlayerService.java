package controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import model.Player;
import model.QueriedPlayers;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PlayerService implements Serializable {
    
    private static PlayerService instance;
    
    @XmlElement
    private List<Player> players = new ArrayList<>();

    public static PlayerService getInstance(){
        if (instance == null) {
            instance = new PlayerService();
        }
        return instance;
    }
    
    
    public PlayerService() {
        
        Player p1 = new Player();
        p1.setNev("Fallen");
        p1.setCsapat_id(2);
        p1.setOlesek(211);
        p1.setRating((float) 4.32);
        p1.setHalalok(10);
        p1.setPozicio("B");
        players.add(p1);
        
        Player p2 = new Player();
        p2.setNev("Device");
        p2.setCsapat_id(4);
        p2.setOlesek(211);
        p2.setRating((float) 4.11);
        p2.setHalalok(55);
        p2.setPozicio("A");
        players.add(p2);
        
        Player p3 = new Player();
        p3.setNev("Simple");
        p3.setCsapat_id(2);
        p3.setOlesek(110);
        p3.setRating((float) 2.11);
        p3.setHalalok(23);
        p3.setPozicio("A");
        players.add(p3);
         
    }
    
    public List<Player> getPlayers() {
        return players;
    }
    
    public QueriedPlayers getQueriedPlayers(String pozicio){
        QueriedPlayers local = new QueriedPlayers();
        
        List<Player> queriablePlayers = new ArrayList<>();
        if(pozicio.length()>0) {
            for(Player p : players) {
                if(p.getPozicio().toLowerCase().trim().equals(pozicio.toLowerCase().trim())) {
                    queriablePlayers.add(p);
                    //local.setPlayer(p);
                }
            }
        } else {
            queriablePlayers = players;
        }
        
        if(queriablePlayers.isEmpty()) return null;
        
        Random rnd = new Random();
        int randomNumber = rnd.nextInt(queriablePlayers.size());
        local.setPlayer(queriablePlayers.get(randomNumber));
                
        return local;
    }
    
    
}
