var interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic =
[
    [ "CsapatHozzaadas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#ae7b3f7b315d9c4633756e7a04ab1c66d", null ],
    [ "CsapatModositas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a4c94932244ea549b2701428b707bef98", null ],
    [ "CsapatTorles", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#ae66f45492c5a704d4e9b4a61a67c74ff", null ],
    [ "EsemenyekCsapatonkent", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a4201dd4baa442b054d8bd04df450bfb6", null ],
    [ "EsemenyHozzaadas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a1fd16a38b7c1b5733916c3272fe7c499", null ],
    [ "EsemenyModositas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#af0c67df8324c9b2ec8f82fac45b4b367", null ],
    [ "EsemenyTorles", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a6aad292d97a4624f435ae83ef063db76", null ],
    [ "JatekosHozzaadas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a79321cd3beda8d5dd072d65bfaeb845c", null ],
    [ "JatekosModositas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a6eab15d92c3e268096484f8aaeaafaa7", null ],
    [ "Jatekosok4AtlagFelett", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#ab8ea48219cc48997f5bcd1cf18726b2e", null ],
    [ "JatekosokCsapatonkent", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a4d178578fa1decafb3a6026211aecc47", null ],
    [ "JatekosTorles", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#adb43904aa01cc34451d88abcaeca9ea5", null ],
    [ "OsszesCsapatLekerdezese", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a5ab0728f94b440e8bfc6a8d9fc99d113", null ],
    [ "OsszesEsemenyLekerdezese", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#add153ca210d6d619c45a0e970846abf8", null ],
    [ "OsszesJatekosLekerdezese", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#afc5222c522d12791d97ea28b738c764c", null ],
    [ "OsszesReszvetelLekerdezese", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a4c1fc53ea8ba93833e03f524933bf85a", null ],
    [ "ReszvetelHozzaadas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a8be0115bc6719f897fd7d5d69ae82c7b", null ],
    [ "ReszvetelModositas", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a93a11255f2b689f2f62803d82d47e031", null ],
    [ "ReszvetelTorles", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html#a9f745b91e64190c8ecf5e84e76c0bff1", null ]
];