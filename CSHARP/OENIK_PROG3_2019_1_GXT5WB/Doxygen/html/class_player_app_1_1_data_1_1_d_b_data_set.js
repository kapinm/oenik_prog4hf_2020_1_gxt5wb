var class_player_app_1_1_data_1_1_d_b_data_set =
[
    [ "DBDataSet", "class_player_app_1_1_data_1_1_d_b_data_set.html#a8a09b1c3ed5ef2566dd20993867bc907", null ],
    [ "DBDataSet", "class_player_app_1_1_data_1_1_d_b_data_set.html#a57797fccac8f66d3a9d585094d1f566f", null ],
    [ "Clone", "class_player_app_1_1_data_1_1_d_b_data_set.html#a6674ae40fb861131d2461065f316f856", null ],
    [ "GetSchemaSerializable", "class_player_app_1_1_data_1_1_d_b_data_set.html#a8e7d81d1cf6cbbb4df83c1d1fa94133b", null ],
    [ "InitializeDerivedDataSet", "class_player_app_1_1_data_1_1_d_b_data_set.html#affe551fdaeecff33a2382e06c5b32be5", null ],
    [ "ReadXmlSerializable", "class_player_app_1_1_data_1_1_d_b_data_set.html#a8fa45e63f853060920c6866278ae16ac", null ],
    [ "ShouldSerializeRelations", "class_player_app_1_1_data_1_1_d_b_data_set.html#af260e15619bb083f108a15f0c0ac4679", null ],
    [ "ShouldSerializeTables", "class_player_app_1_1_data_1_1_d_b_data_set.html#a2cb1d2056effd41991888a54c7af85db", null ],
    [ "Relations", "class_player_app_1_1_data_1_1_d_b_data_set.html#af70c368e710f3b61382562fd886d155c", null ],
    [ "SchemaSerializationMode", "class_player_app_1_1_data_1_1_d_b_data_set.html#a6dcd910d75b52db5b7c1d22c13d3d024", null ],
    [ "Tables", "class_player_app_1_1_data_1_1_d_b_data_set.html#a0e891cca9b0f3e873222b792658ddda4", null ]
];