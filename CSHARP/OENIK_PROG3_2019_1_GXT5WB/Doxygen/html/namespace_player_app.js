var namespace_player_app =
[
    [ "App", "namespace_player_app_1_1_app.html", "namespace_player_app_1_1_app" ],
    [ "Data", "namespace_player_app_1_1_data.html", "namespace_player_app_1_1_data" ],
    [ "JavaWeb", "namespace_player_app_1_1_java_web.html", "namespace_player_app_1_1_java_web" ],
    [ "Logic", "namespace_player_app_1_1_logic.html", "namespace_player_app_1_1_logic" ],
    [ "Program", "namespace_player_app_1_1_program.html", "namespace_player_app_1_1_program" ],
    [ "Repository", "namespace_player_app_1_1_repository.html", "namespace_player_app_1_1_repository" ]
];