﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Logic.Interfaces
{
    using System.Collections.Generic;
    using PlayerApp.Data;

    /// <summary>
    /// ilogic interface.
    /// </summary>
    internal interface ILogic
    {
        // Csapat funkciok

        /// <summary>
        /// Returns all the teams as a list.
        /// </summary>
        /// <returns>List of teams.</returns>
        List<Csapat> OsszesCsapatLekerdezese();

        /// <summary>
        /// Ads a team.
        /// </summary>
        /// <param name="cs">Team as an object.</param>
        void CsapatHozzaadas(Csapat cs);

        /// <summary>
        /// Deletes a team.
        /// </summary>
        /// <param name="id">id of a team.</param>
        void CsapatTorles(int id);

        /// <summary>
        /// modify a team.
        /// </summary>
        /// <param name="cs">team as an object.</param>
        void CsapatModositas(Csapat cs);

        // Esemeny funkciok

        /// <summary>
        /// Returns all the events as a list.
        /// </summary>
        /// <returns>list of events.</returns>
        List<Esemeny> OsszesEsemenyLekerdezese();

        /// <summary>
        /// adds an event.
        /// </summary>
        /// <param name="e">event as an object.</param>
        void EsemenyHozzaadas(Esemeny e);

        /// <summary>
        /// deletes an event.
        /// </summary>
        /// <param name="id">id of an event.</param>
        void EsemenyTorles(int id);

        /// <summary>
        /// modifies an event.
        /// </summary>
        /// <param name="e">event as an object.</param>
        void EsemenyModositas(Esemeny e);

        // Jatekos funkciok

        /// <summary>
        /// Returns all the players as a list.
        /// </summary>
        /// <returns>list of players.</returns>
        List<Jatekos> OsszesJatekosLekerdezese();

        /// <summary>
        /// adds a player.
        /// </summary>
        /// <param name="j">player as an object.</param>
        void JatekosHozzaadas(Jatekos j);

        /// <summary>
        /// deletes a player.
        /// </summary>
        /// <param name="id">id of a player.</param>
        void JatekosTorles(int id);

        /// <summary>
        /// modifies a player.
        /// </summary>
        /// <param name="j">player as an object.</param>
        void JatekosModositas(Jatekos j);

        // Részvétel funkciok

        /// <summary>
        /// Returns all the participation as a list.
        /// </summary>
        /// <returns>list of participations.</returns>
        List<Reszvetel> OsszesReszvetelLekerdezese();

        /// <summary>
        /// adds a participation.
        /// </summary>
        /// <param name="r">participation as an object.</param>
        void ReszvetelHozzaadas(Reszvetel r);

        /// <summary>
        /// deletes a participation.
        /// </summary>
        /// <param name="id">id of a participation.</param>
        void ReszvetelTorles(int id);

        /// <summary>
        /// modifies a participation.
        /// </summary>
        /// <param name="r">participation as an object.</param>
        void ReszvetelModositas(Reszvetel r);

        /// <summary>
        /// participation per team.
        /// </summary>
        /// <returns>string participation.</returns>
        string EsemenyekCsapatonkent();

        /// <summary>
        /// Returns all the players with 4.0 ratings or above.
        /// </summary>
        /// <returns>players as strings.</returns>
        string Jatekosok4AtlagFelett();

        /// <summary>
        /// Returns all the teams with their player counts.
        /// </summary>
        /// <returns>teams as strings.</returns>
        string JatekosokCsapatonkent();
    }
}
