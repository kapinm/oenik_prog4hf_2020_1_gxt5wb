﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PlayerApp.ConsoleClient
{

    public class Player
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public override string ToString()
        {
            return $"ID={Id}\tBrand={Name}\tModel={Name}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://localhost:55958/api/PlayerApi";
            Console.WriteLine("WAITING...");
            Console.ReadLine();


            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Player>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();


                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Player.Name), "Coldzera");
                postData.Add(nameof(Player.Id), "1");
           

                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int playerId = JsonConvert.DeserializeObject<List<Player>>(json).Single(x => x.Name == "Coldzera").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Player.Id), playerId.ToString());
                postData.Add(nameof(Player.Name), "Olof");
                postData.Add(nameof(Player.Name), "2");

                response = client.PostAsync(url + "mod",
                    new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();

                Console.WriteLine("DEL: " + client.GetStringAsync(url + "del/" + playerId).Result);
                Console.WriteLine("ALL: " + client.GetStringAsync(url + "all").Result);
                Console.ReadLine();

            }
        }
    }
}