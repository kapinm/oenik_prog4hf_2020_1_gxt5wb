﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Logic
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PlayerApp.Data;
    using PlayerApp.Logic.Interfaces;
    using PlayerApp.Repository;

    /// <summary>
    /// Business Logic class.
    /// </summary>
    public class BL : ILogic
    {
        private CsapatRepository csapatRepo;
        private EsemenyRepository esemenyRepo;
        private JatekosRepository jatekosRepo;
        private ReszvetelRepository reszvetelRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BL"/> class.
        /// </summary>
        public BL()
        {
            this.csapatRepo = new CsapatRepository();
            this.esemenyRepo = new EsemenyRepository();
            this.jatekosRepo = new JatekosRepository();
            this.reszvetelRepo = new ReszvetelRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BL"/> class.
        /// </summary>
        /// <param name="csapatRepo">team repository.</param>
        /// <param name="esemenyRepo">event repository.</param>
        /// <param name="jatekosRepo">player repository.</param>
        /// <param name="reszvetelRepo">particip. repository.</param>
        public BL(CsapatRepository csapatRepo, EsemenyRepository esemenyRepo, JatekosRepository jatekosRepo, ReszvetelRepository reszvetelRepo)
        {
            this.csapatRepo = csapatRepo;
            this.esemenyRepo = esemenyRepo;
            this.jatekosRepo = jatekosRepo;
            this.reszvetelRepo = reszvetelRepo;
        }

        /// <summary>
        /// Returns all the teams.
        /// </summary>
        /// <returns>List of teams.</returns>
        public List<Csapat> OsszesCsapatLekerdezese()
        {
            return this.csapatRepo.OsszesLekerese();
        }

        /// <summary>
        /// Adds a team.
        /// </summary>
        /// <param name="cs">Team.</param>
        public void CsapatHozzaadas(Csapat cs)
        {
            this.csapatRepo.Hozzaadas(cs);
        }

        /// <summary>
        /// Deletes a team.
        /// </summary>
        /// <param name="id">ID of the team.</param>
        public void CsapatTorles(int id)
        {
            this.csapatRepo.Torles(id);
        }

        /// <summary>
        /// Modifies a team.
        /// </summary>
        /// <param name="csapat">team object.</param>
        public void CsapatModositas(Csapat csapat)
        {
            this.csapatRepo.Modositas(csapat);
        }

        /// <summary>
        /// Returns all the events.
        /// </summary>
        /// <returns>List of events.</returns>
        public List<Esemeny> OsszesEsemenyLekerdezese()
        {
            return this.esemenyRepo.OsszesLekerese();
        }

        /// <summary>
        /// Adds an event.
        /// </summary>
        /// <param name="e">Event.</param>
        public void EsemenyHozzaadas(Esemeny e)
        {
            this.esemenyRepo.Hozzaadas(e);
        }

        /// <summary>
        /// Delets an event.
        /// </summary>
        /// <param name="id">Event ID.</param>
        public void EsemenyTorles(int id)
        {
            this.esemenyRepo.Torles(id);
        }

        /// <summary>
        /// Modifies an event.
        /// </summary>
        /// <param name="e">Event as an object.</param>
        public void EsemenyModositas(Esemeny e)
        {
            this.esemenyRepo.Modositas(e);
        }

        /// <summary>
        /// Returns all the players as a list.
        /// </summary>
        /// <returns>List of Players.</returns>
        public List<Jatekos> OsszesJatekosLekerdezese()
        {
            return this.jatekosRepo.OsszesLekerese();
        }

        /// <summary>
        /// Adds a player.
        /// </summary>
        /// <param name="j">Player object.</param>
        public void JatekosHozzaadas(Jatekos j)
        {
            this.jatekosRepo.Hozzaadas(j);
        }

        /// <summary>
        /// Delets a Player.
        /// </summary>
        /// <param name="id">ID of the player.</param>
        public void JatekosTorles(int id)
        {
            this.jatekosRepo.Torles(id);
        }

        /// <summary>
        /// Modifies a Player.
        /// </summary>
        /// <param name="j">Player Object.</param>
        public void JatekosModositas(Jatekos j)
        {
            this.jatekosRepo.Modositas(j);
        }

        /// <summary>
        /// Returns all the participations.
        /// </summary>
        /// <returns>List of particiaptions.</returns>
        public List<Reszvetel> OsszesReszvetelLekerdezese()
        {
            return this.reszvetelRepo.OsszesLekerese();
        }

        /// <summary>
        /// Ads a participation.
        /// </summary>
        /// <param name="r">Participation object.</param>
        public void ReszvetelHozzaadas(Reszvetel r)
        {
            this.reszvetelRepo.Hozzaadas(r);
        }

        /// <summary>
        /// Deletes a participation.
        /// </summary>
        /// <param name="id">ID of participation.</param>
        public void ReszvetelTorles(int id)
        {
            this.reszvetelRepo.Torles(id);
        }

        /// <summary>
        /// Modifies a participation.
        /// </summary>
        /// <param name="r">Participation object.</param>
        public void ReszvetelModositas(Reszvetel r)
        {
            this.reszvetelRepo.Modositas(r);
        }

        /// <summary>
        /// Returns all .
        /// </summary>
        /// <returns>EventsPerTeam.</returns>
        public string EsemenyekCsapatonkent()
        {
            var query = from csapat in this.csapatRepo.OsszesLekerese()
                        join reszvetelek in this.reszvetelRepo.OsszesLekerese() on csapat.Id equals reszvetelek.Csapat_id
                        join esemeny in this.esemenyRepo.OsszesLekerese() on reszvetelek.Esemeny_id equals esemeny.Id
                        group esemeny.Id by csapat.Nev into e
                        select new
                        {
                            csapatNev = e.Key,
                            esemenyekCsapatonkent = e.Count(),
                        };

            StringBuilder sb = new StringBuilder();

            foreach (var item in query)
            {
                sb.Append(item.csapatNev.ToString() + " (" + item.esemenyekCsapatonkent + ")\n");
            }

            return sb.ToString();
        }

        /// <summary>
        /// players above 4 rating.
        /// </summary>
        /// <returns>players above 4.0 rating.</returns>
        public string Jatekosok4AtlagFelett()
        {
            var query = from jatekos in this.jatekosRepo.OsszesLekerese()
                        orderby jatekos.Rating descending
                        where jatekos.Rating > 4.0
                        select jatekos;

            StringBuilder sb = new StringBuilder();

            foreach (var item in query)
             {
                 sb.Append(item.Nev + " (" + item.Rating.ToString() + ")\n");
             }

            return sb.ToString();
        }

        /// <summary>
        /// players per team.
        /// </summary>
        /// <returns>players per team as a string.</returns>
        public string JatekosokCsapatonkent()
        {
            var query = from csapat in this.csapatRepo.OsszesLekerese()
                        join jatekosok in this.jatekosRepo.OsszesLekerese() on csapat.Id equals jatekosok.Csapat_id
                        group jatekosok.Nev by csapat.Nev into e
                        select new
                        {
                            csapatNev = e.Key,
                            jatekosokSzama = e.Count(),
                        };

            StringBuilder sb = new StringBuilder();

            foreach (var item in query)
            {
                sb.Append(item.csapatNev.ToString() + " (" + item.jatekosokSzama + ")\n");
            }

            return sb.ToString();
        }
    }
}
