var class_player_app_1_1_logic_1_1_b_l =
[
    [ "BL", "class_player_app_1_1_logic_1_1_b_l.html#aea102ec1c3b03bce1de26ddd5c0049c5", null ],
    [ "BL", "class_player_app_1_1_logic_1_1_b_l.html#a3f0c6eb50bd5a50cded189f7b257f985", null ],
    [ "CsapatHozzaadas", "class_player_app_1_1_logic_1_1_b_l.html#a7aebd1ca10413951b09b5c27de4ed3f5", null ],
    [ "CsapatModositas", "class_player_app_1_1_logic_1_1_b_l.html#a1de5a979b4c4114eda6069ebb110e4b0", null ],
    [ "CsapatTorles", "class_player_app_1_1_logic_1_1_b_l.html#abfe3efe26382494ca5af5adf41650b06", null ],
    [ "EsemenyekCsapatonkent", "class_player_app_1_1_logic_1_1_b_l.html#a32dc6cb822230f2ae87a1b5713f92b9d", null ],
    [ "EsemenyHozzaadas", "class_player_app_1_1_logic_1_1_b_l.html#adb0528ffb451c4d42982f365b85aaed3", null ],
    [ "EsemenyModositas", "class_player_app_1_1_logic_1_1_b_l.html#a871d061888809c8ab0c528d66e6c3f98", null ],
    [ "EsemenyTorles", "class_player_app_1_1_logic_1_1_b_l.html#a59cf5f30e32de2751773d4879655d23f", null ],
    [ "JatekosHozzaadas", "class_player_app_1_1_logic_1_1_b_l.html#a3179fda1d067bc66e7eba74448f6af4e", null ],
    [ "JatekosModositas", "class_player_app_1_1_logic_1_1_b_l.html#a0e1dc17843e74f9565dca9dc73af4cb1", null ],
    [ "Jatekosok4AtlagFelett", "class_player_app_1_1_logic_1_1_b_l.html#a3cc44734b295efd7ae4fcb06dc486ca4", null ],
    [ "JatekosokCsapatonkent", "class_player_app_1_1_logic_1_1_b_l.html#a3e8f8b98e953c58a56e03a64992d2489", null ],
    [ "JatekosTorles", "class_player_app_1_1_logic_1_1_b_l.html#af82a76b52f9413345e43b6e4d1b7a751", null ],
    [ "OsszesCsapatLekerdezese", "class_player_app_1_1_logic_1_1_b_l.html#ab876bca20d4912d5316df79b5c427c2e", null ],
    [ "OsszesEsemenyLekerdezese", "class_player_app_1_1_logic_1_1_b_l.html#adacf7a34cc5385c9e5b9813adae40289", null ],
    [ "OsszesJatekosLekerdezese", "class_player_app_1_1_logic_1_1_b_l.html#ae7e8a3b644e4cf011fccb9d14cd815cd", null ],
    [ "OsszesReszvetelLekerdezese", "class_player_app_1_1_logic_1_1_b_l.html#a6a6df6a161f918e58285feb4eb8c2174", null ],
    [ "ReszvetelHozzaadas", "class_player_app_1_1_logic_1_1_b_l.html#a05c19a271b9cf8d8f541d4437fd9c6e1", null ],
    [ "ReszvetelModositas", "class_player_app_1_1_logic_1_1_b_l.html#a18d8d7891f8c5d9e69b29371eb6d2533", null ],
    [ "ReszvetelTorles", "class_player_app_1_1_logic_1_1_b_l.html#a162b57c3685e7f28708176eb083d443b", null ]
];