package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name ="player")
@XmlAccessorType(XmlAccessType.FIELD)
public class Player implements Serializable {
    private static int nextID;
    @XmlElement
    private int id;
    @XmlElement
    private String nev;
    @XmlElement
    private float rating;
    @XmlElement
    private int olesek;
    @XmlElement
    private int halalok;
    @XmlElement
    private int csapat_id;
    @XmlElement
    private String pozicio;

    public Player() {
        this.id = nextID++;
    }
    
    public int getID(){
        return this.id;
    }

    public String getNev() {
        return nev;
    }

    public void setNev(String nev) {
        this.nev = nev;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getOlesek() {
        return olesek;
    }

    public void setOlesek(int olesek) {
        this.olesek = olesek;
    }

    public int getHalalok() {
        return halalok;
    }

    public void setHalalok(int halalok) {
        this.halalok = halalok;
    }

    public int getCsapat_id() {
        return csapat_id;
    }

    public void setCsapat_id(int csapat_id) {
        this.csapat_id = csapat_id;
    }

    public String getPozicio() {
        return pozicio;
    }

    public void setPozicio(String pozicio) {
        this.pozicio = pozicio;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Player other = (Player) obj;
        if (this.id != other.id) {
            return false;
        }
        return true;
    }
    
    
}
