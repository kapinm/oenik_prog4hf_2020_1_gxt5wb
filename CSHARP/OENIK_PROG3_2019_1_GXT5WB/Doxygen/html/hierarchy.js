var hierarchy =
[
    [ "PlayerApp.Data.Csapat", "class_player_app_1_1_data_1_1_csapat.html", null ],
    [ "PlayerApp.Data.Data", "class_player_app_1_1_data_1_1_data.html", null ],
    [ "DataSet", null, [
      [ "PlayerApp.Data.DBDataSet", "class_player_app_1_1_data_1_1_d_b_data_set.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "PlayerApp.Data.Model", "class_player_app_1_1_data_1_1_model.html", null ]
    ] ],
    [ "PlayerApp.Data.Esemeny", "class_player_app_1_1_data_1_1_esemeny.html", null ],
    [ "PlayerApp.Logic.Interfaces.ILogic", "interface_player_app_1_1_logic_1_1_interfaces_1_1_i_logic.html", [
      [ "PlayerApp.Logic.BL", "class_player_app_1_1_logic_1_1_b_l.html", null ]
    ] ],
    [ "PlayerApp.Repository.Interfaces.IRepository< TEntity >", "interface_player_app_1_1_repository_1_1_interfaces_1_1_i_repository.html", null ],
    [ "PlayerApp.Repository.Interfaces.IRepository< Csapat >", "interface_player_app_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "PlayerApp.Repository.CsapatRepository", "class_player_app_1_1_repository_1_1_csapat_repository.html", null ]
    ] ],
    [ "PlayerApp.Repository.Interfaces.IRepository< Esemeny >", "interface_player_app_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "PlayerApp.Repository.EsemenyRepository", "class_player_app_1_1_repository_1_1_esemeny_repository.html", null ]
    ] ],
    [ "PlayerApp.Repository.Interfaces.IRepository< Jatekos >", "interface_player_app_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "PlayerApp.Repository.JatekosRepository", "class_player_app_1_1_repository_1_1_jatekos_repository.html", null ]
    ] ],
    [ "PlayerApp.Repository.Interfaces.IRepository< Reszvetel >", "interface_player_app_1_1_repository_1_1_interfaces_1_1_i_repository.html", [
      [ "PlayerApp.Repository.ReszvetelRepository", "class_player_app_1_1_repository_1_1_reszvetel_repository.html", null ]
    ] ],
    [ "PlayerApp.Data.Jatekos", "class_player_app_1_1_data_1_1_jatekos.html", null ],
    [ "ObjectDumper", "class_object_dumper.html", null ],
    [ "PlayerApp.Program.Program", "class_player_app_1_1_program_1_1_program.html", null ],
    [ "PlayerApp.App.Program", "class_player_app_1_1_app_1_1_program.html", null ],
    [ "PlayerApp.JavaWeb.RandomPlayerPerPosition", "class_player_app_1_1_java_web_1_1_random_player_per_position.html", null ],
    [ "PlayerApp.Data.Reszvetel", "class_player_app_1_1_data_1_1_reszvetel.html", null ],
    [ "PlayerApp.Logic.Tests.Tests", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html", null ]
];