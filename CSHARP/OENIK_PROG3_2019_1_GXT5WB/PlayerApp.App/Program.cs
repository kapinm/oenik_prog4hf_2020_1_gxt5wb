﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.App
{
    using System;
    using System.Collections.Generic;
    using PlayerApp.Data;
    using PlayerApp.JavaWeb;
    using PlayerApp.Logic;

    /// <summary>
    /// Startup class.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main.
        /// </summary>
        /// <param name="args">Startup args.</param>
        public static void Main(string[] args)
        {
            // Business Logic Init
            BL logic = new BL();

            MenuLista();
            Console.WriteLine("Várom az utasítást...");

            while (true)
            {
                string command = Console.ReadLine();

                if (command == "exit")
                {
                    break;
                }

                switch (command)
                {
                    case "menu":
                        MenuLista();
                        break;

                    case "csapat_lista":
                        List<Csapat> csapatok = logic.OsszesCsapatLekerdezese();

                        Console.WriteLine("\nCsapatok listája");
                        Console.WriteLine("-------------------\n");
                        foreach (Csapat csapat in csapatok)
                        {
                            Console.WriteLine(csapat.Nev + " (ID: " + csapat.Id + ", Ország: " + csapat.Orszag + ", Rang: " + csapat.Rang + ", Legerősebb map: " + csapat.Legerosebb_map + ", Legerősebb játékos: " + csapat.Legerosebb_jatekos + ")");
                        }

                        break;

                    case "csapat_hozzaadas":

                        Csapat ujCsapat = new Csapat();

                        Console.Write("Új csapat neve: ");
                        ujCsapat.Nev = Console.ReadLine();
                        Console.Write("Új csapat Rangja: ");
                        ujCsapat.Rang = int.Parse(Console.ReadLine());
                        Console.Write("Új csapat származási országa: ");
                        ujCsapat.Orszag = Console.ReadLine();
                        Console.Write("Új csapat legjobb játékosa: ");
                        ujCsapat.Legerosebb_jatekos = Console.ReadLine();
                        Console.Write("Új csapat legerősebb pályája: ");
                        ujCsapat.Legerosebb_map = Console.ReadLine();

                        logic.CsapatHozzaadas(ujCsapat);
                        WriteInGreen("OK - Új csapat hozzáadása sikeres.");

                        break;

                    case "csapat_torles":

                        Console.Write("Törlendő csapat ID-ja: ");
                        int csapatId = int.Parse(Console.ReadLine());
                        logic.CsapatTorles(csapatId);
                        WriteInGreen("OK - Csapat törlése sikeres.");

                        break;

                    case "csapat_modositas":

                        Csapat modositottCsapat = new Csapat();
                        Console.Write("Módosítandó csapat ID-ja: ");
                        modositottCsapat.Id = int.Parse(Console.ReadLine());

                        Console.Write("Módosított csapat neve (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottCsapat.Nev = Console.ReadLine();
                        Console.Write("Módosított csapat rangja (írj 0-át ha nem szeretnél változtatni): ");
                        modositottCsapat.Rang = int.Parse(Console.ReadLine());
                        Console.Write("Módosított csapat országa (hagyd üresen ha nem szeretnéd változtatni):  ");
                        modositottCsapat.Orszag = Console.ReadLine();
                        Console.Write("Módosított csapat legjobb játékosa (hagyd üresen ha nem szeretnéd változtatni):  ");
                        modositottCsapat.Legerosebb_jatekos = Console.ReadLine();
                        Console.Write("Módosított csapat legjobb pályája (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottCsapat.Legerosebb_map = Console.ReadLine();

                        logic.CsapatModositas(modositottCsapat);
                        WriteInGreen("OK - Csapat módosítása sikeres");

                        break;
                    case "esemeny_lista":
                        List<Esemeny> esemenyek = logic.OsszesEsemenyLekerdezese();

                        Console.WriteLine("\nEsemények listája");
                        Console.WriteLine("-------------------\n");
                        foreach (Esemeny esemeny in esemenyek)
                        {
                            Console.WriteLine(esemeny.Nev + " (ID: " + esemeny.Id + ", Helyszin: " + esemeny.Helyszin + ", Dátum: " + esemeny.Datum + ", Major-e: " + esemeny.Major_e + ", Prize Pool: " + esemeny.Prize_pool + ")");
                        }

                        break;

                    case "esemeny_hozzaadas":

                        Esemeny ujEsemeny = new Esemeny();

                        Console.Write("Új esemény neve: ");
                        ujEsemeny.Nev = Console.ReadLine();
                        Console.Write("Új esemény helyszíne: ");
                        ujEsemeny.Helyszin = Console.ReadLine();
                        Console.Write("Új esemény dátuma: ");
                        ujEsemeny.Datum = DateTime.Parse(Console.ReadLine());
                        Console.Write("Új esemény Major-e: ");
                        ujEsemeny.Major_e = bool.Parse(Console.ReadLine());
                        Console.Write("Új esemény prize_poolja: ");
                        ujEsemeny.Prize_pool = int.Parse(Console.ReadLine());

                        logic.EsemenyHozzaadas(ujEsemeny);
                        WriteInGreen("OK - Új esemény hozzáadása sikeres.");

                        break;

                    case "esemeny_torles":

                        Console.Write("Törlendő esemény ID-ja: ");
                        int esemenyId = int.Parse(Console.ReadLine());
                        logic.EsemenyTorles(esemenyId);
                        WriteInGreen("OK - Esemény törlése sikeres.");

                        break;
                    case "esemeny_modositasa":

                        Esemeny modositottEsemeny = new Esemeny();
                        Console.Write("Módosítandó esemény ID-ja: ");
                        modositottEsemeny.Id = int.Parse(Console.ReadLine());

                        Console.Write("Módosított esemény neve (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottEsemeny.Nev = Console.ReadLine();
                        Console.Write("Módosított esemény dátuma (írj 0-át ha nem szeretnél változtatni): ");
                        modositottEsemeny.Datum = DateTime.Parse(Console.ReadLine());
                        Console.Write("Módosított esemény helyszine (hagyd üresen ha nem szeretnéd változtatni):  ");
                        modositottEsemeny.Helyszin = Console.ReadLine();
                        Console.Write("Módosított esemény Major-e (hagyd üresen ha nem szeretnéd változtatni):  ");
                        modositottEsemeny.Major_e = bool.Parse(Console.ReadLine());
                        Console.Write("Módosított esemény Prize poolja: (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottEsemeny.Prize_pool = int.Parse(Console.ReadLine());

                        logic.EsemenyModositas(modositottEsemeny);
                        WriteInGreen("OK - Csapat módosítása sikeres");

                        break;

                    case "jatekos_lista":
                        List<Jatekos> jatekosok = logic.OsszesJatekosLekerdezese();

                        Console.WriteLine("\nJátékosok listája");
                        Console.WriteLine("-------------------\n");
                        foreach (Jatekos jatekos in jatekosok)
                        {
                            Console.WriteLine(jatekos.Nev + " (ID: " + jatekos.Id + ", Rating: " + jatekos.Rating + ", Ölések: " + jatekos.Olesek + ", Halálok: " + jatekos.Halalok + ", csapat id: " + jatekos.Csapat_id + ",Pozició: " + jatekos.Pozicio + ")");
                        }

                        break;

                    case "jatekos_hozzaadas":

                        Jatekos ujJatekos = new Jatekos();

                        Console.Write("Új Játékos neve: ");
                        ujJatekos.Nev = Console.ReadLine();
                        Console.Write("Új Játékos Ratingje: ");
                        ujJatekos.Rating = double.Parse(Console.ReadLine());
                        Console.Write("Új Játékos ölése: ");
                        ujJatekos.Olesek = int.Parse(Console.ReadLine());
                        Console.Write("Új játékos halála: ");
                        ujJatekos.Halalok = int.Parse(Console.ReadLine());
                        Console.Write("Új játékos csapat idje: ");
                        ujJatekos.Csapat_id = int.Parse(Console.ReadLine());
                        Console.Write("Új játékos poziciója: ");
                        ujJatekos.Pozicio = Console.ReadLine();

                        logic.JatekosHozzaadas(ujJatekos);
                        WriteInGreen("OK - Új Játékos hozzáadása sikeres.");

                        break;

                    case "jatekos_torles":

                        Console.Write("Törlendő játékos ID-ja: ");
                        int jatekosId = int.Parse(Console.ReadLine());
                        logic.JatekosTorles(jatekosId);
                        WriteInGreen("OK - Játékos törlése sikeres.");

                        break;
                    case "jatekos_modositasa":

                        Jatekos modositottJatekos = new Jatekos();
                        Console.Write("Módosítandó játékos ID-ja: ");
                        modositottJatekos.Id = int.Parse(Console.ReadLine());

                        Console.Write("Módosított játékos neve (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottJatekos.Nev = Console.ReadLine();
                        Console.Write("Módosított játékos ratingje (írj 0-át ha nem szeretnél változtatni): ");

                        modositottJatekos.Rating = int.Parse(Console.ReadLine());
                        Console.Write("Módosított játékos ölése (hagyd üresen ha nem szeretnéd változtatni):  ");
                        modositottJatekos.Olesek = int.Parse(Console.ReadLine());
                        Console.Write("Módosított játékos halála (hagyd üresen ha nem szeretnéd változtatni):  ");
                        modositottJatekos.Halalok = int.Parse(Console.ReadLine());
                        Console.Write("Módosított játékos csapat idje (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottJatekos.Csapat_id = int.Parse(Console.ReadLine());
                        Console.Write("Módosított játékos csapat poziciója (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottJatekos.Pozicio = Console.ReadLine();

                        logic.JatekosModositas(modositottJatekos);
                        WriteInGreen("OK - Játékos módosítása sikeres");

                        break;
                    case "reszvetel_lista":
                        List<Reszvetel> reszvetelek = logic.OsszesReszvetelLekerdezese();

                        Console.WriteLine("\nRészvételek listája");
                        Console.WriteLine("-------------------\n");
                        foreach (Reszvetel reszvetel in reszvetelek)
                        {
                            Console.WriteLine(" (ID: " + reszvetel.Id + ", esemény id: " + reszvetel.Esemeny_id + ", csapat id: " + reszvetel.Csapat_id + ", helyezés: " + reszvetel.Helyezes);
                        }

                        break;

                    case "reszvetel_hozzaadas":

                        Reszvetel ujReszvetel = new Reszvetel();

                        Console.Write("Új részvétel esemény id-ja: ");
                        ujReszvetel.Esemeny_id = int.Parse(Console.ReadLine());
                        Console.Write("Új részvétel csapat id-ja: ");
                        ujReszvetel.Csapat_id = int.Parse(Console.ReadLine());
                        Console.Write("Új részvétel helyezése: ");
                        ujReszvetel.Helyezes = int.Parse(Console.ReadLine());

                        logic.ReszvetelHozzaadas(ujReszvetel);
                        WriteInGreen("OK - Új Részvétel hozzáadása sikeres.");

                        break;

                    case "reszvetel_torles":

                        Console.Write("Törlendő részvétel ID-ja: ");
                        int reszvetelId = int.Parse(Console.ReadLine());
                        logic.ReszvetelTorles(reszvetelId);
                        WriteInGreen("OK - Részvétel törlése sikeres.");

                        break;

                    case "reszvetel_modositas":

                        Reszvetel modositottReszvetel = new Reszvetel();
                        Console.Write("Módosítandó részvétel ID-ja: ");
                        modositottReszvetel.Id = int.Parse(Console.ReadLine());

                        Console.Write("Módosított részvétel esemény idja: (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottReszvetel.Esemeny_id = int.Parse(Console.ReadLine());
                        Console.Write("Módosított részvétel csapat idja: (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottReszvetel.Csapat_id = int.Parse(Console.ReadLine());
                        Console.Write("Módosított részvétel helyezése: (hagyd üresen ha nem szeretnéd változtatni): ");
                        modositottReszvetel.Helyezes = int.Parse(Console.ReadLine());
                        logic.ReszvetelModositas(modositottReszvetel);
                        WriteInGreen("OK - Részvétel módosítása sikeres");

                        break;

                    case "random_jatekos_java":

                        Console.Write("Játékos pozíciója (A/B): ");
                        string pozicio = Console.ReadLine();

                        RandomPlayerPerPosition p = new RandomPlayerPerPosition();
                        p.getRandomPlayerPerPosition(pozicio);

                        break;

                    case "jatekosok_4_atlag_felett":

                        Console.WriteLine(logic.Jatekosok4AtlagFelett());

                        break;

                    case "jatekosok_szama_csapatonkent":

                        Console.WriteLine(logic.JatekosokCsapatonkent());

                        break;

                    case "esemenyek_szama_csapatonkent":

                        Console.WriteLine(logic.EsemenyekCsapatonkent());

                        break;

                    default:
                        Console.WriteLine("Helytelen utasítás!");
                        break;
                }

                Console.WriteLine("\nVárom a következő utasítást...");
            }
        }

        /// <summary>
        /// Writes out all the menu options.
        /// </summary>
        protected static void MenuLista()
        {
            Console.WriteLine("\n\n--- MENÜ ---");
            Console.WriteLine("- exit: kilépés");
            Console.WriteLine("- menu: menü kiírása");
            Console.WriteLine("\n--- CRUD ---");
            Console.WriteLine("- csapat_lista: csapatok listázása");
            Console.WriteLine("- csapat_hozzaadas: csapat hozzáadása");
            Console.WriteLine("- csapat_torles: csapat törlése");
            Console.WriteLine("- csapat_modositas: csapat módosítása");
            Console.WriteLine("---");
            Console.WriteLine("- esemeny_lista: események listázása");
            Console.WriteLine("- esemeny_hozzaadas: esemény hozzáadása");
            Console.WriteLine("- esemeny_torles: esemény törlése");
            Console.WriteLine("- esemeny_modositasa: esemény módosítása");
            Console.WriteLine("---");
            Console.WriteLine("- jatekos_lista: játékos listázása");
            Console.WriteLine("- jatekos_hozzaadas: játékos hozzáadása");
            Console.WriteLine("- jatekos_torles: játékos törlése");
            Console.WriteLine("- jatekos_modositasa: játékos módosítása");
            Console.WriteLine("---");
            Console.WriteLine("- reszvetel_lista: részvétel listázása");
            Console.WriteLine("- reszvetel_hozzaadas: részvétel hozzáadása");
            Console.WriteLine("- reszvetel_torles: részvétel törlése");
            Console.WriteLine("- reszvetel_modositasa: részvétel módosítása");
            Console.WriteLine("---");
            Console.WriteLine("- esemenyek_szama_csapatonkent: hány rendezványen vettek részt a csapatok");
            Console.WriteLine("- jatekosok_4_atlag_felett: 4-es átlagpont feletti játékosok");
            Console.WriteLine("- jatekosok_szama_csapatonkent: igazolt játékosok száma csapatonként");
            Console.WriteLine("---");
            Console.WriteLine("- random_jatekos_java: random játékos lekérdezése Java végpontról");
            Console.WriteLine("\n\n");
        }

        /// <summary>
        /// Writes in green.
        /// </summary>
        /// <param name="s">Text to write out.</param>
        protected static void WriteInGreen(string s)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(s);
            Console.ResetColor();
        }
    }
}
