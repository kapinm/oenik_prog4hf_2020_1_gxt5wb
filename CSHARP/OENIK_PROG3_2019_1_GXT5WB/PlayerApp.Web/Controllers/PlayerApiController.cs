﻿using PlayerApp.Logic;
using PlayerApp.Data;
using System.Collections.Generic;
using System.Web.Http;
using System.Reflection;

namespace PlayerApp.Web.Controllers
{
    public class PlayerApiController : ApiController
    {
        public class ApiResult // Single-point-of-use?
        {
            public bool OperationResult { get; set; }
        }

        BL logic;
        IMapper mapper;
        public PlayerApiController()
        {
            //CarDbContext ctx = new CarDbContext();
            //CarRepository carRepo = new CarRepository(ctx);
            //BrandRepository brandRepo = new BrandRepository(ctx);
            //logic = new CarLogic(carRepo, brandRepo);
            //mapper = MapperFactory.CreateMapper();
        }


        [ActionName("all")]
        [HttpGet]

        public IEnumerable<Module> GetAll()
        {
            var jatekosok = logic.JatekosokCsapatonkent();
            return mapper.Map<IList<Data.Jatekos>, List<Jatekos>>(jatekosok);
        }

    
        //[ActionName("del")]
        //[HttpGet]
        //public ApiResult DelOnePlayer(int id)
        //{
        //    bool success = logic.JatekosTorles(id);
        //    return new ApiResult() { OperationResult = success }; 
        //}

 
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOnePlayer(Jatekos jatekos)
        {
            logic.JatekosHozzaadas(jatekos);
            return new ApiResult() { OperationResult = true };
        }


        //[HttpPost]
        //[ActionName("mod")]
        //public ApiResult ModOnePlayer(Jatekos jatekos)
        //{
        //    //bool success = logic.JatekosModositas(jatekos);
        //    //return new ApiResult() { OperationResult = success };
        //}
 
    }
}
