﻿// <copyright file="ReszvetelRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using PlayerApp.Data;
    using PlayerApp.Repository.Interfaces;

    /// <summary>
    /// Repository of particaption.
    /// </summary>
    public class ReszvetelRepository : IRepository<Reszvetel>
    {
        private Model db;
        private DBDataSet set;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReszvetelRepository"/> class.
        /// </summary>
        public ReszvetelRepository()
        {
            this.db = new Model();
            this.set = new DBDataSet();
        }

        /// <summary>
        /// adss participation.
        /// </summary>
        /// <param name="item">participation object.</param>
        public virtual void Hozzaadas(Reszvetel item)
        {
            this.db.Reszvetel.Add(item);
            this.db.SaveChanges();
        }

        /// <summary>
        /// deletes a participation by id.
        /// </summary>
        /// <param name="id">id of a participation.</param>
        public virtual void Torles(int id)
        {
            this.db.Reszvetel.Remove(this.LekeresById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// query of all participations.
        /// </summary>
        /// <returns>list of participations.</returns>
        public virtual List<Reszvetel> OsszesLekerese()
        {
            return this.db.Reszvetel.ToList();
        }

        /// <summary>
        /// participation by id.
        /// </summary>
        /// <param name="id">id.</param>
        /// <returns>participation object.</returns>
        public virtual Reszvetel LekeresById(int id)
        {
            return this.db.Reszvetel.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// modifies a participation.
        /// </summary>
        /// <param name="r">participation object.</param>
        public virtual void Modositas(Reszvetel r)
        {
            Reszvetel reszvetel = this.LekeresById(r.Id);
            if (reszvetel != null)
            {
                if (r.Esemeny_id != 0)
                {
                    r.Esemeny_id = r.Esemeny_id;
                }

                if (r.Csapat_id != 0)
                {
                    r.Csapat_id = r.Csapat_id;
                }

                if (r.Helyezes != 0)
                {
                    r.Helyezes = r.Helyezes;
                }
            }

            this.db.SaveChanges();
        }
    }
}
