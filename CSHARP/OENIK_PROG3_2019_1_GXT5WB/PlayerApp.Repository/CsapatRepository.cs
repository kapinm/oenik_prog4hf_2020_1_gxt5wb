﻿// <copyright file="CsapatRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Remoting.Contexts;
    using System.Text;
    using System.Threading.Tasks;
    using PlayerApp.Data;
    using PlayerApp.Repository.Interfaces;

    /// <summary>
    /// repository for team.
    /// </summary>
    public class CsapatRepository : IRepository<Csapat>
    {
        private Model db;
        private DBDataSet set;

        /// <summary>
        /// Initializes a new instance of the <see cref="CsapatRepository"/> class.
        /// </summary>
        public CsapatRepository()
        {
            this.db = new Model();
            this.set = new DBDataSet();
        }

        /// <summary>
        /// Ads a team.
        /// </summary>
        /// <param name="item">Team object.</param>
        public virtual void Hozzaadas(Csapat item)
        {
            this.db.Csapat.Add(item);
            this.db.SaveChanges();
        }

        /// <summary>
        /// deletes a team.
        /// </summary>
        /// <param name="id">id of the player.</param>
        public virtual void Torles(int id)
        {
            this.db.Csapat.Remove(this.LekeresById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// Query of all teams.
        /// </summary>
        /// <returns>list of teams.</returns>
        public virtual List<Csapat> OsszesLekerese()
        {
            return this.db.Csapat.ToList();
        }

        /// <summary>
        /// query a team by id.
        /// </summary>
        /// <param name="id">id of the team.</param>
        /// <returns>team as an object.</returns>
        public virtual Csapat LekeresById(int id)
        {
            return this.db.Csapat.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// modifies a team.
        /// </summary>
        /// <param name="cs">team object.</param>
        public virtual void Modositas(Csapat cs)
        {
            Csapat csapat = this.LekeresById(cs.Id);
            if (csapat != null)
            {
                if (cs.Nev != string.Empty)
                {
                    csapat.Nev = cs.Nev;
                }

                if (cs.Legerosebb_jatekos != string.Empty)
                {
                    csapat.Legerosebb_jatekos = cs.Legerosebb_jatekos;
                }

                if (cs.Orszag != string.Empty)
                {
                    csapat.Orszag = cs.Orszag;
                }

                if (cs.Legerosebb_map != string.Empty)
                {
                    csapat.Legerosebb_map = cs.Legerosebb_map;
                }

                if (cs.Rang != 0)
                {
                    csapat.Rang = cs.Rang;
                }
            }

            this.db.SaveChanges();
        }
    }
}
