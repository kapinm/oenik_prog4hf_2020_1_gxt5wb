﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http;
using System.Xml.Linq;
using PlayerApp.Data;

namespace PlayerApp.JavaWeb
{

    public class RandomPlayerPerPosition
    {

        private string V = @"http://localhost:17286/wphf/RandomPlayerPerPositionServlet?pozicio=";

        /// <summary>
        /// Returns a random player with its stats from an API endpoint
        /// </summary>
        /// <param name="position">Position of the player as string</param>
        public void getRandomPlayerPerPosition(string position)
        {

            V += position;

            using (var client = new WebClient())
            {
                string xml_text = client.DownloadString(V);
                XDocument doc = XDocument.Parse(xml_text);
                Console.WriteLine(doc);
            }

        }
 
    }
}
