var namespace_player_app_1_1_repository =
[
    [ "Interfaces", "namespace_player_app_1_1_repository_1_1_interfaces.html", "namespace_player_app_1_1_repository_1_1_interfaces" ],
    [ "CsapatRepository", "class_player_app_1_1_repository_1_1_csapat_repository.html", "class_player_app_1_1_repository_1_1_csapat_repository" ],
    [ "EsemenyRepository", "class_player_app_1_1_repository_1_1_esemeny_repository.html", "class_player_app_1_1_repository_1_1_esemeny_repository" ],
    [ "JatekosRepository", "class_player_app_1_1_repository_1_1_jatekos_repository.html", "class_player_app_1_1_repository_1_1_jatekos_repository" ],
    [ "ReszvetelRepository", "class_player_app_1_1_repository_1_1_reszvetel_repository.html", "class_player_app_1_1_repository_1_1_reszvetel_repository" ]
];