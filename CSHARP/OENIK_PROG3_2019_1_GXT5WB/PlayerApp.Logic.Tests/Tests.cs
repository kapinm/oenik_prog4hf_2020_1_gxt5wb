﻿// <copyright file="Tests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PlayerApp.Data;
    using PlayerApp.Logic;
    using PlayerApp.Repository;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class Tests
    {
        private Mock<CsapatRepository> csapatRepo;
        private Mock<EsemenyRepository> esemenyRepo;
        private Mock<JatekosRepository> jatekosRepo;
        private Mock<ReszvetelRepository> reszvetelRepo;

        private List<Csapat> csapatok;
        private List<Jatekos> jatekosok;
        private List<Esemeny> esemenyek;
        private List<Reszvetel> reszvetelek;

        private BL bl;

        /// <summary>
        /// Sets up the test environment.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.csapatRepo = new Mock<CsapatRepository>();
            this.esemenyRepo = new Mock<EsemenyRepository>();
            this.jatekosRepo = new Mock<JatekosRepository>();
            this.reszvetelRepo = new Mock<ReszvetelRepository>();

            this.csapatok = new List<Csapat>()
            {
                new Csapat()
                {
                    Id = 1, Nev = "SK", Rang = 1, Legerosebb_jatekos = "Olofmaiser", Legerosebb_map = "dust2", Orszag = "Sweden",
                },
                new Csapat()
                {
                    Id = 2, Nev = "Astralis", Rang = 2, Legerosebb_jatekos = "Device", Legerosebb_map = "nuke", Orszag = "Denmark",
                },
            };
            this.esemenyek = new List<Esemeny>()
            {
                new Esemeny()
                {
                    Id = 1, Nev = "ESWC", Datum = DateTime.Parse("2018.01.01."), Helyszin = "Párizs", Major_e = true, Prize_pool = 100000,
                },
                new Esemeny()
                {
                    Id = 2, Nev = "ESL Katowice", Datum = DateTime.Parse("2018.12.08."), Helyszin = "Katowice", Major_e = false, Prize_pool = 200000,
                },
            };
            this.jatekosok = new List<Jatekos>()
            {
                new Jatekos()
                {
                   Id = 1, Nev = "Fallen", Csapat_id = 1, Olesek = 100, Halalok = 100, Pozicio = "A", Rating = 3.4,
                },
                new Jatekos()
                {
                   Id = 2, Nev = "TACO", Csapat_id = 1, Olesek = 110, Halalok = 23, Pozicio = "B", Rating = 4.8,
                },
            };
            this.reszvetelek = new List<Reszvetel>()
            {
                new Reszvetel()
                {
                    Id = 1, Csapat_id = 1, Esemeny_id = 2, Helyezes = 1,
                },
                new Reszvetel()
                {
                    Id = 2, Csapat_id = 2, Esemeny_id = 2, Helyezes = 2,
                },
            };

            this.csapatRepo.Setup(x => x.OsszesLekerese()).Returns(this.csapatok);
            this.esemenyRepo.Setup(x => x.OsszesLekerese()).Returns(this.esemenyek);
            this.jatekosRepo.Setup(x => x.OsszesLekerese()).Returns(this.jatekosok);
            this.reszvetelRepo.Setup(x => x.OsszesLekerese()).Returns(this.reszvetelek);

            this.csapatRepo.Setup(x => x.Hozzaadas(It.IsAny<Csapat>())).Callback(() =>
            {
                this.csapatok.Add(new Csapat());
            });
            this.esemenyRepo.Setup(x => x.Hozzaadas(It.IsAny<Esemeny>())).Callback(() =>
            {
                this.esemenyek.Add(new Esemeny());
            });
            this.jatekosRepo.Setup(x => x.Hozzaadas(It.IsAny<Jatekos>())).Callback(() =>
            {
                this.jatekosok.Add(new Jatekos());
            });
            this.reszvetelRepo.Setup(x => x.Hozzaadas(It.IsAny<Reszvetel>())).Callback(() =>
            {
                this.reszvetelek.Add(new Reszvetel());
            });

            this.bl = new BL(this.csapatRepo.Object, this.esemenyRepo.Object, this.jatekosRepo.Object, this.reszvetelRepo.Object);
        }

        /// <summary>
        /// Test for the team listing.
        /// </summary>
        [Test]
        public void OsszesCsapatLekerdezese()
        {
            var item = this.bl.OsszesCsapatLekerdezese();
            Assert.That(item.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// Test for the team addition.
        /// </summary>
        [Test]
        public void CsapatHozzaadas()
        {
            var currentCount = this.bl.OsszesCsapatLekerdezese().Count;

            Csapat cs = new Csapat();
            cs.Nev = "uj";
            cs.Rang = 2;
            cs.Orszag = "HU";

            this.bl.CsapatHozzaadas(cs);

            Assert.That(currentCount, Is.Not.EqualTo(this.bl.OsszesCsapatLekerdezese().Count));
            Assert.That(currentCount, Is.EqualTo(2));
            Assert.That(this.bl.OsszesCsapatLekerdezese().Count, Is.EqualTo(3));
        }

        /// <summary>
        /// Test for the participation query.
        /// </summary>
        [Test]
        public void OsszesEsemenyLekerdezese()
        {
            var item = this.bl.OsszesEsemenyLekerdezese();
            Assert.That(item.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// test for the participation adding.
        /// </summary>
        [Test]
        public void EsemenyHozzaadas()
        {
            var currentCount = this.bl.OsszesEsemenyLekerdezese().Count;

            Esemeny cs = new Esemeny();
            cs.Nev = "uj";
            cs.Helyszin = "London";
            cs.Major_e = true;
            cs.Prize_pool = 1000;

            this.bl.EsemenyHozzaadas(cs);

            Assert.That(currentCount, Is.Not.EqualTo(this.bl.OsszesEsemenyLekerdezese().Count));
            Assert.That(currentCount, Is.EqualTo(2));
            Assert.That(this.bl.OsszesEsemenyLekerdezese().Count, Is.EqualTo(3));
        }

        /// <summary>
        /// test for querying players.
        /// </summary>
        [Test]
        public void OsszesJatekosLekerdezese()
        {
            var item = this.bl.OsszesJatekosLekerdezese();
            Assert.That(item.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// test for adding player.
        /// </summary>
        [Test]
        public void JatekosHozzaadas()
        {
            var currentCount = this.bl.OsszesJatekosLekerdezese().Count;

            Jatekos cs = new Jatekos();
            cs.Nev = "uj";
            cs.Olesek = 100;
            cs.Rating = 4.3;
            cs.Pozicio = "A";

            this.bl.JatekosHozzaadas(cs);

            Assert.That(currentCount, Is.Not.EqualTo(this.bl.OsszesJatekosLekerdezese().Count));
            Assert.That(currentCount, Is.EqualTo(2));
            Assert.That(this.bl.OsszesJatekosLekerdezese().Count, Is.EqualTo(3));
        }

        /// <summary>
        /// test for query all the participation.
        /// </summary>
        [Test]
        public void OsszesReszvetelLekerdezese()
        {
            var item = this.bl.OsszesReszvetelLekerdezese();
            Assert.That(item.Count, Is.EqualTo(2));
        }

        /// <summary>
        /// test for adding a participation.
        /// </summary>
        [Test]
        public void ReszvetelHozzaadas()
        {
            var currentCount = this.bl.OsszesReszvetelLekerdezese().Count;

            Reszvetel cs = new Reszvetel();
            cs.Esemeny_id = 1;
            cs.Csapat_id = 1;
            this.bl.ReszvetelHozzaadas(cs);

            Assert.That(currentCount, Is.Not.EqualTo(this.bl.OsszesReszvetelLekerdezese().Count));
            Assert.That(currentCount, Is.EqualTo(2));
            Assert.That(this.bl.OsszesReszvetelLekerdezese().Count, Is.EqualTo(3));
        }

        /*        [Test]
                public void CsapatTorlese()
                {
                    this.bl.CsapatTorles(1);
                    Assert.That(this.bl.OsszesCsapatLekerdezese().Count, Is.EqualTo(1));
                }
        */

        /// <summary>
        /// test for all the players above 4 rating.
        /// </summary>
        [Test]
        public void Jatekosok4AtlagFelett()
        {
            string result;
            result = this.bl.Jatekosok4AtlagFelett();

            Assert.That(result.Contains("TACO"), Is.True);
        }

        /// <summary>
        /// test for  players per team.
        /// </summary>
        [Test]
        public void JatekosokCsapatonkent()
        {
            string result;
            result = this.bl.JatekosokCsapatonkent();

            Assert.That(result.Contains("SK"), Is.True);
            Assert.That(result.Contains("(2)"), Is.True);
        }

        /// <summary>
        /// test for participation per team.
        /// </summary>
        [Test]
        public void EsemenyekCsapatonkent()
        {
            string result;
            result = this.bl.EsemenyekCsapatonkent();

            Assert.That(result.Contains("SK"), Is.True);
            Assert.That(result.Contains("(1)"), Is.True);
            Assert.That(result.Contains("Astralis"), Is.True);
            Assert.That(result.Contains("(1)"), Is.True);
        }
    }
}
