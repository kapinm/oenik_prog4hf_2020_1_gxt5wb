var class_player_app_1_1_logic_1_1_tests_1_1_tests =
[
    [ "CsapatHozzaadas", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a9f11623583bc9340582c0a25060d5fe3", null ],
    [ "EsemenyekCsapatonkent", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a3603db7974611dae59f6c9afa7c4a35b", null ],
    [ "EsemenyHozzaadas", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a737e7527b41729c4c0c95bd021129299", null ],
    [ "JatekosHozzaadas", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a342736c7591c5933cda9492f4914f448", null ],
    [ "Jatekosok4AtlagFelett", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#add66a290f757fe98840d9e3f928a8a40", null ],
    [ "JatekosokCsapatonkent", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a1c490d172c59b53445f06672d957c7b6", null ],
    [ "OsszesCsapatLekerdezese", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#af9b9128a6046adaed501eddbb20e6736", null ],
    [ "OsszesEsemenyLekerdezese", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a204458fd7d72b93c64a8f3a715c5b47a", null ],
    [ "OsszesJatekosLekerdezese", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#aa6daed0d13fd40bb9e3e2558568bca4a", null ],
    [ "OsszesReszvetelLekerdezese", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#ac44bdbad1e2cc817aa55d0baa526218b", null ],
    [ "ReszvetelHozzaadas", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#ad482c8ea9e385df2a1d15ffb6363f1d1", null ],
    [ "Setup", "class_player_app_1_1_logic_1_1_tests_1_1_tests.html#a2b9ec26d577407581ec9b2a2da9b5239", null ]
];