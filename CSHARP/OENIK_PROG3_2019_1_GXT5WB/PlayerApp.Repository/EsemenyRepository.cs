﻿// <copyright file="EsemenyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Remoting.Contexts;
    using System.Text;
    using System.Threading.Tasks;
    using PlayerApp.Data;
    using PlayerApp.Repository.Interfaces;

    /// <summary>
    /// repository for event.
    /// </summary>
    public class EsemenyRepository : IRepository<Esemeny>
    {
        private Model db;
        private DBDataSet set;

        /// <summary>
        /// Initializes a new instance of the <see cref="EsemenyRepository"/> class.
        /// </summary>
        public EsemenyRepository()
        {
            this.db = new Model();
            this.set = new DBDataSet();
        }

        /// <summary>
        /// adds a participation.
        /// </summary>
        /// <param name="item">participation object.</param>
        public virtual void Hozzaadas(Esemeny item)
        {
            this.db.Esemeny.Add(item);
            this.db.SaveChanges();
        }

        /// <summary>
        /// deletes a participation by id.
        /// </summary>
        /// <param name="id">id of the participation.</param>
        public virtual void Torles(int id)
        {
            this.db.Esemeny.Remove(this.LekeresById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// query all the participations.
        /// </summary>
        /// <returns>list of participations.</returns>
        public virtual List<Esemeny> OsszesLekerese()
        {
            return this.db.Esemeny.ToList();
        }

        /// <summary>
        /// query of a participation by id.
        /// </summary>
        /// <param name="id">id of a participation.</param>
        /// <returns>participation object.</returns>
        public virtual Esemeny LekeresById(int id)
        {
            return this.db.Esemeny.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// modifies a participation.
        /// </summary>
        /// <param name="e">participation object.</param>
        public virtual void Modositas(Esemeny e)
        {
            Esemeny esemeny = this.LekeresById(e.Id);
            if (esemeny != null)
            {
                if (e.Nev != string.Empty)
                {
                    e.Nev = e.Nev;
                }

                if (e.Helyszin != string.Empty)
                {
                    e.Helyszin = e.Helyszin;
                }

                if (e.Datum != null)
                {
                    e.Datum = e.Datum;
                }

                if (e.Major_e != null)
                {
                    e.Major_e = e.Major_e;
                }

                if (e.Prize_pool != 0)
                {
                    e.Prize_pool = e.Prize_pool;
                }
            }

            this.db.SaveChanges();
        }
    }
}
