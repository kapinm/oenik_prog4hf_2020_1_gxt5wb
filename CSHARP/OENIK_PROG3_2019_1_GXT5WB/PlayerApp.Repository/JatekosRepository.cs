﻿// <copyright file="JatekosRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Remoting.Contexts;
    using System.Text;
    using System.Threading.Tasks;
    using PlayerApp.Data;
    using PlayerApp.Repository.Interfaces;

    /// <summary>
    /// repository for player.
    /// </summary>
    public class JatekosRepository : IRepository<Jatekos>
    {
        private Model db;
        private DBDataSet set;

        /// <summary>
        /// Initializes a new instance of the <see cref="JatekosRepository"/> class.
        /// </summary>
        public JatekosRepository()
        {
            this.db = new Model();
            this.set = new DBDataSet();
        }

        /// <summary>
        /// adds a player.
        /// </summary>
        /// <param name="item">player object.</param>
        public virtual void Hozzaadas(Jatekos item)
        {
            this.db.Jatekos.Add(item);
            this.db.SaveChanges();
        }

        /// <summary>
        /// deletes a player.
        /// </summary>
        /// <param name="id">id of a plyer.</param>
        public virtual void Torles(int id)
        {
            this.db.Jatekos.Remove(this.LekeresById(id));
            this.db.SaveChanges();
        }

        /// <summary>
        /// query of all players.
        /// </summary>
        /// <returns>list of players.</returns>
        public virtual List<Jatekos> OsszesLekerese()
        {
            return this.db.Jatekos.ToList();
        }

        /// <summary>
        /// query of a plyer by id.
        /// </summary>
        /// <param name="id">id of a player.</param>
        /// <returns>player object.</returns>
        public virtual Jatekos LekeresById(int id)
        {
            return this.db.Jatekos.Where(x => x.Id == id).FirstOrDefault();
        }

        /// <summary>
        /// modifies a player.
        /// </summary>
        /// <param name="j">player object.</param>
        public virtual void Modositas(Jatekos j)
        {
            Jatekos jatekos = this.LekeresById(j.Id);
            if (jatekos != null)
            {
                if (j.Nev != string.Empty)
                {
                    j.Nev = j.Nev;
                }

                if (j.Rating != 0)
                {
                    j.Rating = j.Rating;
                }

                if (j.Olesek != 0)
                {
                    j.Olesek = j.Olesek;
                }

                if (j.Halalok != 0)
                {
                    j.Halalok = j.Halalok;
                }

                if (j.Csapat_id != 0)
                {
                    j.Csapat_id = j.Csapat_id;
                }

                if (j.Pozicio != string.Empty)
                {
                    j.Pozicio = j.Pozicio;
                }
            }

            this.db.SaveChanges();
        }

        public void Torles(bool id)
        {
            throw new NotImplementedException();
        }
    }
}
