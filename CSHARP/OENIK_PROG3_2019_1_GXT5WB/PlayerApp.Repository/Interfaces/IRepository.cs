﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PlayerApp.Repository.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// defines crud.
    /// </summary>
    /// <typeparam name="TEntity">tentity.</typeparam>
    internal interface IRepository<TEntity>
    {
        /// <summary>
        /// Adds an Entity.
        /// </summary>
        /// <param name="item">Entity as object.</param>
        void Hozzaadas(TEntity item);

        /// <summary>
        /// Delets an Entity.
        /// </summary>
        /// <param name="id">Entity as object.</param>
        void Torles(int id);

        /// <summary>
        /// Returns all the Entities as a list.
        /// </summary>
        /// <returns>List of Entities.</returns>
        List<TEntity> OsszesLekerese();

        /// <summary>
        /// Returns an Entity with an ID.
        /// </summary>
        /// <param name="id">ID of the entity.</param>
        /// <returns>id.</returns>
        TEntity LekeresById(int id);

        /// <summary>
        /// Modifies an Entity.
        /// </summary>
        /// <param name="item">Entity to modify.</param>
        void Modositas(TEntity item);
    }
}
